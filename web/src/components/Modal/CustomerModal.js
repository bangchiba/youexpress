import React, { Component } from 'react'

export class CustomerModal extends Component {
  render() {
    return (
      <div className="modal fade">
        <div className="modal-dialog">
            <div className="modal-content">
            <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                <button className="close" data-dismiss="modal">
                <span>&times;</span>
                </button>
            </div>
            <div className="modal-body">
            </div>
            <div className="modal-footer">
                <button className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button className="btn btn-primary">Save changes</button>
            </div>
            </div>
        </div>
    </div>
    )
  }
}

export default CustomerModal
