import React, { Component } from 'react';
import { connect } from "react-redux";
import AuthService from '../../actions/API/AuthService';

export class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: '', 
            password: ''
        }

        this.onChange = this.onChange.bind(this);
        this.toggle = this.toggle.bind(this);
        this.handleSubmitLogin = this.handleSubmitLogin.bind(this);

        this.Auth = new AuthService();
    }

    onChange(event){
        return this.setState({[event.target.name]: event.target.value});
    }

    handleSubmitLogin(event){
        event.preventDefault();
        this.Auth.login(this.state.email, this.state.password)
        .then(res => {
            this.props.history.replace('/');
        })
        .catch(err => {
            alert(err)
        })
    }

    render() {
        return (
            <div className="container">
                <div className="card card-login mx-auto mt-5">
                <div className="card-header">Login</div>
                <div className="card-body">
                    <form>
                    <div className="form-group">
                        <div className="form-label-group">
                        <input type="email" id="inputEmail" className="form-control" placeholder="Email address" required="required" autoFocus="autofocus"/>
                        <label htmlFor="inputEmail">Email address</label>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="form-label-group">
                        <input type="password" id="inputPassword" className="form-control" placeholder="Password" required="required"/>
                        <label htmlFor="inputPassword">Password</label>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="checkbox">
                        <label>
                            <input type="checkbox" value="remember-me"/>
                            Remember Password
                        </label>
                        </div>
                    </div>
                    <a className="btn btn-primary btn-block" href="index.html">Login</a>
                    </form>
                    <div className="text-center">
                    <a className="d-block small mt-3" href="register.html">Register an Account</a>
                    <a className="d-block small" href="forgot-password.html">Forgot Password?</a>
                    </div>
                </div>
                </div>
            </div>
        )
    }
}

export default connect(null, {})(Login);
