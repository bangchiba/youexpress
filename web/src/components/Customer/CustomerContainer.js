import React, { Component } from 'react';
import Navbar from '../Global/Navbar';
import Sidebar from '../Global/Sidebar';
import Footer from '../Global/Footer';
import Customer from "./Customer";
import CustomerAdd from "./CustomerAdd";
import CustomerEdit from "./CustomerEdit";

export class CustomerContainer extends Component {

  renderCompent() {
    switch(this.props.match.path){
      case '/customer':
        return <Customer/>
      case '/customer/add':
        return <CustomerAdd history={this.props.history}/>
      case '/customer/edit/:id':
        return <CustomerEdit param={this.props.match.params} history={this.props.history}/>
      default:
        return ""
    }
  }

  render() {
    return (
        <div>
            <Navbar/>  
            <div id="wrapper">
                <Sidebar/>
    
                <div id="content-wrapper">
                    { this.renderCompent()}
                    <Footer/>
                </div>
            </div>
        </div>
    )
  }
}

export default CustomerContainer
