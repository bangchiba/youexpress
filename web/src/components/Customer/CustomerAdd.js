import React, { Component } from 'react'
import { Link } from "react-router-dom";
import {API_URL} from '../../config';
import axios from 'axios';

export default class CustomerAdd extends Component {
    constructor(props){
        super(props);
        this.state = {
            name : "",
            address : "",
            phone : "",
            email: ""
        }
        this.onChange = this.onChange.bind(this);
        this.addCustomer = this.addCustomer.bind(this);
    }

    onChange(e){
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    addCustomer(){
        let data = {
            name: this.state.name,
            address: this.state.address,
            phone: this.state.phone,
            email: this.state.email
        }
        axios.post(`${API_URL}/customer`,data)
        .then(res => {
          console.log(res);
          if(res.status === 201){
            this.props.history.push("/customer");
          }
        }).catch( err => {
          console.log(err);
        });
    }
  render() {
    return (
      <div className="container-fluid">

        <ol className="breadcrumb">
          <li className="breadcrumb-item">
          <Link to="/customer">Customer</Link>
          </li>
          <li className="breadcrumb-item active">Add Customer</li>
        </ol>
        <div className="col-md-8 offset-md-2">
        <div className="card mb-3">
          <div className="card-header">
            <i className="fas fa-table"></i>
            Add Customer
        </div>
          <div className="card-body">
            <div className="form-group">
                <label htmlFor="">Nama</label>
                <input name="name" type="text" className="form-control" onChange={this.onChange} value={this.state.name}/>
            </div>
            <div className="form-group">
                <label htmlFor="">Alamat</label>
                <input type="text" className="form-control" name="address" onChange={this.onChange} value={this.state.address}/>
            </div>
            <div className="form-group">
                <label htmlFor="">No. Telpon</label>
                <input type="text" className="form-control" name="phone" onChange={this.onChange} value={this.state.phone}/>
            </div>
            <div className="form-group">
                <label htmlFor="">Email</label>
                <input type="text" className="form-control" name="email" onChange={this.onChange} value={this.state.email}/>
            </div>
            <button className="btn btn-primary float-right" onClick={this.addCustomer}>Save</button>
          </div>
          <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
        </div>
      </div>
    )
  }
}
