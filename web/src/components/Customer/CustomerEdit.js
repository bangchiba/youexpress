import React, { Component } from 'react'
import { Link } from "react-router-dom";
import axios from 'axios';
import {API_URL} from '../../config';

class CustomerEdit extends Component {
    constructor(props){
        super(props);
        this.state = {
            name : "",
            address : "",
            phone : "",
            email: ""
        }
        this.onChange = this.onChange.bind(this);
        this.updateCustomer = this.updateCustomer.bind(this);
    }
    
    componentWillMount() {
        let id = this.props.param.id;
        
        axios.get(`${API_URL}/customer/${id}/edit`)
        .then(res => {
          let {name, address, phone, email} = res.data.data;
          this.setState({
            name, address, phone, email
          })
        })
    }

    onChange(e){
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    updateCustomer(){
        let id = this.props.param.id;
        let data = {
            name: this.state.name,
            address: this.state.address,
            phone: this.state.phone,
            email: this.state.email
        }

        axios.put(`${API_URL}/customer/${id}`,data)
        .then(res => {
          if(res.status === 201){
            this.props.history.push("/customer");
          }
        }).catch( err => {
          console.log(err);
        });
    }
  render() {
    return (
      <div className="container-fluid">

        <ol className="breadcrumb">
          <li className="breadcrumb-item">
          <Link to="/customer">Customer</Link>
          </li>
          <li className="breadcrumb-item active">Edit Customer</li>
        </ol>
        <div className="col-md-8 offset-md-2">
        <div className="card mb-3">
          <div className="card-header">
            <i className="fas fa-table"></i>
            Edit Customer
        </div>
          <div className="card-body">
            <div className="form-group">
                <label htmlFor="">Nama</label>
                <input name="name" type="text" className="form-control" onChange={this.onChange} value={this.state.name}/>
            </div>
            <div className="form-group">
                <label htmlFor="">Alamat</label>
                <input type="text" className="form-control" name="address" onChange={this.onChange} value={this.state.address}/>
            </div>
            <div className="form-group">
                <label htmlFor="">No. Telpon</label>
                <input type="text" className="form-control" name="phone" onChange={this.onChange} value={this.state.phone}/>
            </div>
            <div className="form-group">
                <label htmlFor="">Email</label>
                <input type="text" className="form-control" name="email" onChange={this.onChange} value={this.state.email}/>
            </div>
            <button className="btn btn-primary float-right" onClick={this.updateCustomer}>Simpan</button>
          </div>
          <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
        </div>
      </div>
    )
  }
}

export default CustomerEdit;
