import React, { Component } from 'react';
import { Link } from "react-router-dom";

export class Sidebar extends Component {
  render() {
    return (
        <ul className="sidebar navbar-nav">
          <li className="nav-item active">
            <Link to="/" className="nav-link"><i className="fas fa-fw fa-tachometer-alt"></i><span>Dashboard</span></Link>
          </li>
          <li className="nav-item">
            <Link to="/customer" className="nav-link"><i className="fas fa-fw fa-table"></i><span>Customer</span></Link>
          </li>
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i className="fas fa-fw fa-folder"></i>
              <span>Gudang</span>
            </a>
            <div className="dropdown-menu" aria-labelledby="pagesDropdown">
              <Link className="dropdown-item" to="/block">Blok</Link>
              <Link className="dropdown-item" to="/space">Ruangan</Link>
              <div className="dropdown-divider"></div>
              <h6 className="dropdown-header">Harga:</h6>
              <Link className="dropdown-item" to="/plan">Paket</Link>
              <Link className="dropdown-item" to="/price">Harga</Link>
            </div>
          </li>
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i className="fas fa-fw fa-folder"></i>
              <span>Transaksi</span>
            </a>
            <div className="dropdown-menu" aria-labelledby="pagesDropdown">
              <Link className="dropdown-item" to="/transaction_in">Pemasukan Barang</Link>
              <Link className="dropdown-item" to="/transaction">Pengeluaran Barang</Link>
            </div>
          </li>
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i className="fas fa-fw fa-folder"></i>
              <span>Laporan</span>
            </a>
            <div className="dropdown-menu" aria-labelledby="pagesDropdown">
              <Link className="dropdown-item" to="/block">Pemasukan Barang</Link>
              <Link className="dropdown-item" to="/space">Pengeluaran Barang</Link>
            </div>
          </li>
      </ul>
    )
  }
}

export default Sidebar
