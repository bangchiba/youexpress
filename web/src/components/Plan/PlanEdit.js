import React, { Component } from 'react'
import { Link } from "react-router-dom";
import axios from 'axios';
import {API_URL} from '../../config';

class PlanEdit extends Component {
    constructor(props){
        super(props);
        this.state = {
            name : "",
        }
        this.onChange = this.onChange.bind(this);
        this.updatePlan = this.updatePlan.bind(this);
    }
    
    componentWillMount() {
        let id = this.props.param.id;
        axios.get(`${API_URL}/plan/${id}/edit`)
        .then(res => {
          let {name} = res.data.data;
          this.setState({
            name
          })
        })
    }

    onChange(e){
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    updatePlan(){
        let id = this.props.param.id;
        let data = {
            name: this.state.name
        }

        axios.put(`${API_URL}/plan/${id}`,data)
        .then(res => {
          if(res.status === 201){
            this.props.history.push("/plan");
          }
        }).catch( err => {
          console.log(err);
        });
    }
  render() {
    return (
      <div className="container-fluid">

        <ol className="breadcrumb">
          <li className="breadcrumb-item">
          <Link to="/plan">Paket</Link>
          </li>
          <li className="breadcrumb-item active">Edit Paket</li>
        </ol>
        <div className="col-md-8 offset-md-2">
        <div className="card mb-3">
          <div className="card-header">
            <i className="fas fa-table"></i>
            Edit Paket
        </div>
          <div className="card-body">
            <div className="form-group">
                <label htmlFor="">Nama</label>
                <input name="name" type="text" className="form-control" onChange={this.onChange} value={this.state.name}/>
            </div>
            <button className="btn btn-primary float-right" onClick={this.updatePlan}>Simpan</button>
          </div>
          <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
        </div>
      </div>
    )
  }
}


export default PlanEdit;
