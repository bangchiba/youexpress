import React, { Component } from 'react';
import Navbar from '../Global/Navbar';
import Sidebar from '../Global/Sidebar';
import Footer from '../Global/Footer';
import Plan from "./Plan";
import PlanAdd from "./PlanAdd";
import PlanEdit from "./PlanEdit";

export class PlanContainer extends Component {

  renderCompent() {
    switch(this.props.match.path){
      case '/plan':
        return <Plan/>
      case '/plan/add':
        return <PlanAdd history={this.props.history}/>
      case '/plan/edit/:id':
        return <PlanEdit param={this.props.match.params} history={this.props.history}/>
      default:
        return ""
    }
  }

  render() {
    return (
        <div>
            <Navbar/>  
            <div id="wrapper">
                <Sidebar/>
    
                <div id="content-wrapper">
                    { this.renderCompent()}
                    <Footer/>
                </div>
            </div>
        </div>
    )
  }
}

export default PlanContainer

