import React, { Component } from 'react';
import { Link } from "react-router-dom";
import axios from 'axios';
import {API_URL} from '../../config';
import Swal from 'sweetalert2';

export class Plan extends Component {
  constructor(props){
    super(props);
    this.state = {
      data : [
      ],


    }
  }
  componentWillMount(){
    axios.get(`${API_URL}/plan`)
    .then(res => {
      console.log(res);
      let data = res.data.data;
      this.setState({data})
    })
  }

  render() {
    return (
        <div className="container-fluid">

        <ol className="breadcrumb">
          <li className="breadcrumb-item">
          <Link to="#">plan</Link>
          </li>
          <li className="breadcrumb-item active">List Paket</li>
        </ol>

        <div className="card mb-3">
          <div className="card-header">
            <i className="fas fa-table"></i>
            Data Paket</div>
          <div className="card-body">
            <div className="table-responsive">
            <div className="text-left">
             <Link to="/plan/add"> <button className="btn btn-success"><i className="fas fa-plus"></i> Tambah</button></Link>
            </div>
              <table className="table table-bordered my-4" id="dataTable" width="100%" cellSpacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                {
                  this.state.data.map((d, i) => {

                    return (
                      <tr key={i}>
                        <td align="center">{i+1}</td>
                        <td>{d.name}</td>
                        <td className="text-center">
                        <Link to={`/plan/edit/${d.id}`}><button className="btn btn-primary btn-sm"><i className="fas fa-edit"></i></button></Link>
                          <button className="btn btn-danger btn-sm" onClick={() => this.removeItem(d.id)}><i className="fas fa-trash"></i></button>
                        </td>
                      </tr>
                    )
                  }

                  )
                }
                  
                </tbody>
              </table>
            </div>
          </div>
          <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
      </div>
    )
  }
}

export default Plan
