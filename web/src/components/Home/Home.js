import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Navbar from '../Global/Navbar';
import Sidebar from '../Global/Sidebar';
import Dashboard from '../Dashboard/Dashboard';
import Footer from '../Global/Footer';

class HomeComponent extends Component{

    render(){
        console.log(localStorage.getItem('id_token'));
        
        return (
                <div>
                    <Navbar/>  
                    <div id="wrapper">
                        <Sidebar/>
                        <div id="content-wrapper">
                            <Dashboard/>
                            <Footer/>
                        </div>
                    </div>
                </div>
        )
    }
}

// export default HomeComponent;
export default connect(null, {})(HomeComponent);