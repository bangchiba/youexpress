import React, { Component } from 'react';
import Navbar from '../Global/Navbar';
import Sidebar from '../Global/Sidebar';
import Footer from '../Global/Footer';
import Space from "./Space";
import SpaceAdd from "./SpaceAdd";
import SpaceEdit from "./SpaceEdit";

export class SpaceContainer extends Component {

  renderCompent() {
    switch(this.props.match.path){
      case '/space':
        return <Space/>
      case '/space/add':
        return <SpaceAdd history={this.props.history}/>
      case '/space/edit/:id':
        return <SpaceEdit param={this.props.match.params} history={this.props.history}/>
      default:
        return ""
    }
  }

  render() {
    return (
        <div>
            <Navbar/>  
            <div id="wrapper">
                <Sidebar/>
    
                <div id="content-wrapper">
                    { this.renderCompent()}
                    <Footer/>
                </div>
            </div>
        </div>
    )
  }
}

export default SpaceContainer

