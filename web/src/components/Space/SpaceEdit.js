import React, { Component } from 'react'
import { Link } from "react-router-dom";
import axios from 'axios';
import {API_URL} from '../../config';

class SpaceEdit extends Component {
    constructor(props){
        super(props);
        this.state = {
            master_block_id : "",
            space : "",
            long : "",
            width: "",
            height:"",
            note: "",
            block: []
        }
        this.onChange = this.onChange.bind(this);
        this.updateSpace = this.updateSpace.bind(this);
    }
    
    componentWillMount() {
        axios.get(`${API_URL}/block`)
        .then(res => {
            this.setState({
                block: res.data.data
            })
        });
        
        
        let id = this.props.param.id;
        axios.get(`${API_URL}/space/${id}/edit`)
        .then(res => {
            console.log(res);
            
          let {id, master_block_id, space, block_name, width, long, height,note} = res.data.data;
          this.setState({
            id, master_block_id, space, block_name, width, long, height, note
          })
        })
    }

    onChange(e){
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    updateSpace(){
        let id = this.props.param.id;
        let data = {
            master_block_id : this.state.master_block_id,
            space : this.state.space,
            long : this.state.long,
            width: this.state.width,
            height: this.state.height,
            note: this.state.note,
        }

        axios.put(`${API_URL}/space/${id}`,data)
        .then(res => {
          if(res.status === 201){
            this.props.history.push("/space");
          }
        }).catch( err => {
          console.log(err);
        });
    }
  render() {
    return (
        <div className="container-fluid">

        <ol className="breadcrumb">
          <li className="breadcrumb-item">
          <Link to="/space">Ruangan</Link>
          </li>
          <li className="breadcrumb-item active">Tambah Ruangan</li>
        </ol>
        <div className="col-md-8 offset-md-2">
        <div className="card mb-3">
          <div className="card-header">
            <i className="fas fa-table"></i>
            Tambah Ruangan
        </div>
          <div className="card-body">
            <div className="form-group">
                <label htmlFor="">Nama Blok</label>
                <select className="form-control" name="master_block_id" onChange={this.onChange}>
                    <option value="">Pilih Nama Block</option>
                    {this.state.block.map((d, i) => {
                        return (
                            <option selected={this.state.master_block_id == d.id} key={i} value={d.id}>{d.name}</option>
                        )
                    })}
                </select>
            </div>
            <div className="form-group">
                <label htmlFor="">Ruang</label>
                <input type="text" className="form-control" name="space" onChange={this.onChange} value={this.state.space}/>
            </div>
            <div className="form-group">
                <label htmlFor="">Panjang</label>
                <input type="text" className="form-control" name="long" onChange={this.onChange} value={this.state.long}/>
            </div>
            <div className="form-group">
                <label htmlFor="">Tinggi</label>
                <input type="text" className="form-control" name="height" onChange={this.onChange} value={this.state.height}/>
            </div>
            <div className="form-group">
                <label htmlFor="">Lebar</label>
                <input type="text" className="form-control" name="width" onChange={this.onChange} value={this.state.width}/>
            </div>
            <div className="form-group">
                <label htmlFor="">Keterangan</label>
                <input type="text" className="form-control" name="note" onChange={this.onChange} value={this.state.note}/>
            </div>
            <button className="btn btn-primary float-right" onClick={this.updateSpace}>Save</button>
          </div>
          <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
        </div>
      </div>
    )
  }
}

export default SpaceEdit;
