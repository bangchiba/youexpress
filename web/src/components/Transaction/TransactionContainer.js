import React, { Component } from 'react';
import Navbar from '../Global/Navbar';
import Sidebar from '../Global/Sidebar';
import Footer from '../Global/Footer';
import Transaction from "./Transaction";
import TransactionInput from "./TransactionInput";
import TransactionOut from "./TransactionOut";

export class TransactionContainer extends Component {

  renderCompent() {
    switch(this.props.match.path){
      case '/transaction_in':
        return <Transaction/>
      case '/transaction_in/add':
        return <TransactionInput history={this.props.history}/>
      case '/transaction_in/edit/:id':
        return <TransactionOut param={this.props.match.params} history={this.props.history}/>
      default:
        return ""
    }
  }

  render() {
    return (
        <div>
            <Navbar/>  
            <div id="wrapper">
                <Sidebar/>
    
                <div id="content-wrapper">
                    { this.renderCompent()}
                    <Footer/>
                </div>
            </div>
        </div>
    )
  }
}

export default TransactionContainer;

