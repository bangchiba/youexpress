import React, { Component } from 'react'
import { Link } from "react-router-dom";
import {API_URL} from '../../config';
import axios from 'axios';

export class TransactionInput extends Component {
  constructor(props){
    super(props);
    this.state = {
        customer : [],
        space: [],
        plan:[]
    }
    this.onChange = this.onChange.bind(this);
    this.addCustomer = this.addCustomer.bind(this);
}

componentWillMount(){
  axios.get(`${API_URL}/customer`)
  .then(res => {
    let customer = res.data.data;
    this.setState({customer})
  });

  axios.get(`${API_URL}/option_space`)
  .then(res => {
    let space = res.data.data;
    this.setState({space})
  });

  axios.get(`${API_URL}/option_plan`)
  .then(res => {
    let plan = res.data.data;
    this.setState({plan})
  });

}

onChange(e){
  e.preventDefault();
  
  this.setState({
      [e.target.name]: e.target.value
  });
}

onChangeSpace(e){
    e.preventDefault();
    axios.get(`${API_URL}/option_plan`)
    .then();
}

addCustomer(){
    let data = {
        name: this.state.name,
        address: this.state.address,
        phone: this.state.phone,
        email: this.state.email
    }
    axios.post(`${API_URL}/store_customer`,data)
    .then(res => {
      console.log(res);
      if(res.status === 201){
        this.props.history.push("/customer");
      }
    }).catch( err => {
      console.log(err);
    });
}
  render() {
    return (
      <div className="container-fluid">

        <ol className="breadcrumb">
          <li className="breadcrumb-item">
          <Link to="/customer">Transaksi Masuk</Link>
          </li>
          <li className="breadcrumb-item active">Tambah Transaksi Masuk</li>
        </ol>
        <div className="col-md-8 offset-md-2">
        <div className="card mb-3">
          <div className="card-header">
            <i className="fas fa-table"></i>
            Tambah Transaksi Masuk
        </div>
          <div className="card-body">
            <div className="form-group">
                <label htmlFor="">Nama Customer</label>
                <select className="form-control" name="master_customer_id" onChange={this.onChange}>
                    <option value="">Pilih Nama Customer</option>
                    {this.state.customer.map((d, i) => {
                        return (
                            <option key={i} value={d.id}>{d.name}</option>
                        )
                    })}
                </select>
            </div>
            <div className="form-group">
                <label htmlFor="">Space</label>
                <select className="form-control" name="master_space_id" onChange={this.onChangeSpace}>
                    <option value="">Pilih Space</option>
                    {this.state.space.map((d, i) => {
                        return (
                            <option key={i} value={d.id}>{d.block_name}</option>
                        )
                    })}
                </select>
            </div>
            <div className="form-group">
                <label htmlFor="">Plan</label>
                <select className="form-control" name="master_plan_id" onChange={this.onChange}>
                    <option value="">Pilih Plan</option>
                    {this.state.plan.map((d, i) => {
                        return (
                            <option key={i} value={d.id}>{d.name}</option>
                        )
                    })}
                </select>
            </div>
            <div className="form-group">
                <label htmlFor="">Tanggal Masuk</label>
                <input type="text" className="form-control" name="email" onChange={this.onChange} value={this.state.email}/>
            </div>
            <button className="btn btn-primary float-right" onClick={this.addCustomer}>Save</button>
          </div>
          <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
        </div>
      </div>
    )
  }
}

export default TransactionInput
