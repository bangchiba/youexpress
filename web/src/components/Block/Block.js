import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchBlock } from "../../actions/blockActions";
import {API_URL} from '../../config';
import Swal from 'sweetalert2';

export class Block extends Component {
  constructor(props){
    super(props);
    this.state = {
      data : [
      ],


    }
    // this.removeItem = this.removeItem.bind(this);
  }
  componentWillMount(){
    this.props.fetchBlock();
  }

  removeItem(id){
    // console.log(id);
    // Swal.fire({
    //   title: 'Anda Yakin?',
    //   text: "Data akan terhapus Permanent!",
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Ya, Hapus ini!'
    // }).then((result) => {
    //   if (result.value) {
    //     axios.detele(`${API_URL}/delete_block/${id}`)
    //     .then(res => {
    //       console.log(res);
    //     });
    //     // Swal.fire(
    //     //   'Deleted!',
    //     //   'te',
    //     //   'success'
    //     // )
    //   }
    // })
  }

  render() {
    return (
        <div className="container-fluid">

        <ol className="breadcrumb">
          <li className="breadcrumb-item">
          <Link to="#">Block</Link>
          </li>
          <li className="breadcrumb-item active">List Block</li>
        </ol>

        <div className="card mb-3">
          <div className="card-header">
            <i className="fas fa-table"></i>
            Data Block</div>
          <div className="card-body">
            <div className="table-responsive">
            <div className="text-left">
             <Link to="/block/add"> <button className="btn btn-success"><i className="fas fa-plus"></i> Tambah</button></Link>
            </div>
              <table className="table table-bordered my-4" id="dataTable" width="100%" cellSpacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                {
                  this.props.blocks.map((d, i) => {

                    return (
                      <tr key={i}>
                        <td align="center">{i+1}</td>
                        <td>{d.name}</td>
                        <td className="text-center">
                        <Link to={`/block/edit/${d.id}`}><button className="btn btn-primary btn-sm"><i className="fas fa-edit"></i></button></Link>
                          <button className="btn btn-danger btn-sm" onClick={() => this.removeItem(d.id)}><i className="fas fa-trash"></i></button>
                        </td>
                      </tr>
                    )
                  }

                  )
                }
                  
                </tbody>
              </table>
            </div>
          </div>
          <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        blocks: state.block.items
    }
}

export default connect(mapStateToProps, { fetchBlock })(Block);
