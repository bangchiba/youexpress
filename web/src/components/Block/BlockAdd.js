import React, { Component } from 'react'
import { Link } from "react-router-dom";
import {API_URL} from '../../config';
import axios from 'axios';

export default class BlockAdd extends Component {
    constructor(props){
        super(props);
        this.state = {
            name : "",
        }
        this.onChange = this.onChange.bind(this);
        this.addBlock = this.addBlock.bind(this);
    }

    onChange(e){
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    addBlock(){
        let data = {
            name: this.state.name,
        }
        axios.post(`${API_URL}/block`,data)
        .then(res => {
          console.log(res);
          if(res.status === 201){
            this.props.history.push("/block");
          }
        }).catch( err => {
          console.log(err);
        });
    }
  render() {
    return (
      <div className="container-fluid">

        <ol className="breadcrumb">
          <li className="breadcrumb-item">
          <Link to="/block">Block</Link>
          </li>
          <li className="breadcrumb-item active">Add Block</li>
        </ol>
        <div className="col-md-8 offset-md-2">
        <div className="card mb-3">
          <div className="card-header">
            <i className="fas fa-table"></i>
            Add Block
        </div>
          <div className="card-body">
            <div className="form-group">
                <label htmlFor="">Nama</label>
                <input name="name" type="text" className="form-control" onChange={this.onChange} value={this.state.name}/>
            </div>
            <button className="btn btn-primary float-right" onClick={this.addBlock}>Save</button>
          </div>
          <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
        </div>
      </div>
    )
  }
}
