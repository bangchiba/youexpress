import React, { Component } from 'react';
import Navbar from '../Global/Navbar';
import Sidebar from '../Global/Sidebar';
import Footer from '../Global/Footer';
import Block from "./Block";
import BlockAdd from "./BlockAdd";
import BlockEdit from "./BlockEdit";

export class BlockContainer extends Component {

  renderCompent() {
    switch(this.props.match.path){
      case '/block':
        return <Block/>
      case '/block/add':
        return <BlockAdd history={this.props.history}/>
      case '/block/edit/:id':
        return <BlockEdit param={this.props.match.params} history={this.props.history}/>
      default:
        return ""
    }
  }

  render() {
    return (
        <div>
            <Navbar/>  
            <div id="wrapper">
                <Sidebar/>
    
                <div id="content-wrapper">
                    { this.renderCompent()}
                    <Footer/>
                </div>
            </div>
        </div>
    )
  }
}

export default BlockContainer

