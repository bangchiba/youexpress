import React, { Component } from 'react';
import Navbar from '../Global/Navbar';
import Sidebar from '../Global/Sidebar';
import Footer from '../Global/Footer';
import Price from "./Price";
import PriceAdd from "./PriceAdd";
import PriceEdit from "./PriceEdit";

export class PriceContainer extends Component {

  renderCompent() {
    switch(this.props.match.path){
      case '/price':
        return <Price/>
      case '/price/add':
        return <PriceAdd history={this.props.history}/>
      case '/price/edit/:id':
        return <PriceEdit param={this.props.match.params} history={this.props.history}/>
      default:
        return ""
    }
  }

  render() {
    return (
        <div>
            <Navbar/>  
            <div id="wrapper">
                <Sidebar/>
    
                <div id="content-wrapper">
                    { this.renderCompent()}
                    <Footer/>
                </div>
            </div>
        </div>
    )
  }
}

export default PriceContainer

