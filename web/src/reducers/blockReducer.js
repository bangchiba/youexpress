import { CREATE_BLOCK, FETCH_BLOCK, EDIT_BLOCK } from "../actions/types/blockType";

const initialState = {
    items: [],
    item: {}
}

export default function(state = initialState, action){
    switch (action.type) {
        case FETCH_BLOCK:
            return {
                ...state,
                items: action.payload.data
            }
        case EDIT_BLOCK:
        return {
            ...state,
            item: action.payload.data
        }
        default:
            return state;
    }
}