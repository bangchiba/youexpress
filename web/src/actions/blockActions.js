import { CREATE_BLOCK, FETCH_BLOCK, EDIT_BLOCK } from "./types/blockType";
import { API_URL } from "../../src/config";
import axios from 'axios';

export const fetchBlock = () => dispatch => {
    axios.get(`${API_URL}/block`)
    .then(res => dispatch({
        type: FETCH_BLOCK,
        payload: res.data
    }))
}