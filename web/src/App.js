import React, { Component } from 'react';
import { HashRouter, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import Home from './components/Home/Home';
import Login from './components/Login/Login';
import CustomerContainer from "./components/Customer/CustomerContainer";
import BlockContainer from "./components/Block/BlockContainer";
import SpaceContainer from "./components/Space/SpaceContainer";
import PlanContainer from "./components/Plan/PlanContainer";
import PriceContainer from "./components/Price/PriceContainer";
import TransactionContainer from "./components/Transaction/TransactionContainer";

import store from './store';

class App extends Component {
  render(){
    return (
      <Provider store={store} onUpdate={() => window.scrollTo(0,0)}>
        <HashRouter>
          <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/customer" component={CustomerContainer}/>
              <Route exact path="/customer/add" component={CustomerContainer}/>
              <Route exact path="/customer/edit/:id" component={CustomerContainer}/>
              <Route exact path="/block" component={BlockContainer}/>
              <Route exact path="/block/add" component={BlockContainer}/>
              <Route exact path="/block/edit/:id" component={BlockContainer}/>
              <Route exact path="/space" component={SpaceContainer}/>
              <Route exact path="/space/add" component={SpaceContainer}/>
              <Route exact path="/space/edit/:id" component={SpaceContainer}/>
              <Route exact path="/transaction_in" component={TransactionContainer}/>
              <Route exact path="/transaction_in/add" component={TransactionContainer}/>
              <Route exact path="/transaction/out/:id" component={TransactionContainer}/>
              <Route exact path="/plan" component={PlanContainer}/>
              <Route exact path="/plan/add" component={PlanContainer}/>
              <Route exact path="/plan/edit/:id" component={PlanContainer}/>
              <Route exact path="/price" component={PriceContainer}/>
              <Route exact path="/price/add" component={PriceContainer}/>
              <Route exact path="/price/edit/:id" component={PriceContainer}/>
          </Switch>
      </HashRouter>
    </Provider>
    )
  }
}

export default App;