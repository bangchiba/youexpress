## API
cd api<br>
composer install<br>
php artisan migrate<br>
php artisan db:seed<br>
php artisan passport:install<br>
php artisan serve

## WEB
cd web<br>
npm install<br>
npm start