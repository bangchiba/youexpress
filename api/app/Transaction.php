<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['master_customer_id', 'master_plan_id', 'master_space_id', 'start_date', 'end_date', 'status'];

    public function master_customer()
    {
        return $this->belongsTo(Master_customer::class);
    }

    public function master_space()
    {
        return $this->belongsTo(Master_space::class);
    }

    public function master_plan()
    {
        return $this->belongsTo(Master_plan::class);
    }

    public function getStatusStatusAttribute()
    {
        if($this->status == 1){
            return 'Masuk';
        }else{
            return 'Keluar';
        }
    }
}
