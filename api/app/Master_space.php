<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_space extends Model
{
    protected $fillable = ['master_block_id', 'space', 'long', 'width', 'height', 'status', 'note'];

    public function master_block()
    {
        return $this->belongsTo(Master_block::class);
    }

    public function master_plan()
    {
        return $this->belongsToMany(Master_plan::class, 'master_space_plan_prices')->withPivot('price');
    }

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }

    public function getStatusStatusAttribute()
    {
        if($this->status == 1){
            return 'Kosong';
        }else{
            return 'Terisi';
        }
    }
}
