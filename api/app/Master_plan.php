<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_plan extends Model
{
    protected $fillable = ['name'];

    public function master_space()
    {
        return $this->belongsToMany(Master_space::class, 'master_space_plan_prices');
    }

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }
    
}
