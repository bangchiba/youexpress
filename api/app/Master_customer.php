<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_customer extends Model
{
    protected $fillable = ['name', 'address', 'phone', 'email', 'note'];

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }
}
