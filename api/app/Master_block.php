<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_block extends Model
{
    protected $fillable = ['name'];

    public function master_space()
    {
        return $this->hasMany(Master_space::class);
    }
}
