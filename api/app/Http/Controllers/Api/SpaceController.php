<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\Space_transformer;
use App\Master_space;
use Validator;

class SpaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $space = Master_space::all();
        $result = fractal($space, new Space_transformer)->toArray();
        return response()->json($result, 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'master_block_id' => 'required',
            'space' => 'required',
            'long' => 'required',
            'width' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = $request->all();
        $data['status'] = 1;

        $space = Master_space::create($request->all());

        return response()->json($space, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $space = Master_space::find($id);
        $result = fractal($space, new Space_transformer)->toArray();
        return response()->json($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'master_block_id' => 'required',
            'space' => 'required',
            'long' => 'required',
            'width' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()], 401);
        }

        $space = Master_space::find($id);
        $space->master_block_id = $request->master_block_id;
        $space->space = $request->space;
        $space->long = $request->long;
        $space->width = $request->width;
        $space->height = $request->height;
        $space->note = $request->note;
        $space->save();

        return response()->json(['success' => 'success'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function option_space()
    {
        $space = Master_space::where('status',1)->get();
        $result = fractal($space, new Space_transformer)->toArray();
        return response()->json($result, 201);
    }
}
