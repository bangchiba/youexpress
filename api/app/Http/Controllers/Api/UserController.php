<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request['email'], 'password' => $request['password']])){
            $user = Auth::user();
            $success['token'] = $user->createToken('youexpress')->accessToken;
            return response()->json($success, 201);
        }else{
            return response()->json(['error' => 'Unauthorised'], 401);
        }

    }
}
