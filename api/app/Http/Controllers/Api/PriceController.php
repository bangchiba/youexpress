<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Master_space_plan_price;
use App\Master_space;
use App\Transformers\Space_price_transformer;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $space = Master_space::all();
        $result = fractal($space, new Space_price_transformer())->toArray();

        $arr_result = [];
        foreach ($result['data'] as $key => $val) {
            if(count($val['master_plan']) > 0){
                foreach ($val['master_plan'] as $i => $valp) {
                    $arr_result[$i] = $valp['pivot'];
                    $arr_result[$i]['plan_name'] = $valp['name'];
                    $arr_result[$i]['block_name'] = $val['block_name'];
                    $arr_result[$i]['space'] = $val['space'];
                }
            }
        }
        
        return response()->json($arr_result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
