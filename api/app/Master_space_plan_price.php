<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_space_plan_price extends Model
{
    protected $fillable = ['master_space_id', 'master_plan_id', 'price'];

    // public function master_space()
    // {
    //     return $this->belongsTo(Master_space::class);
    // }

    // public function master_plan()
    // {
    //     return $this->belongsTo(Master_plan::class);
    // }
}
