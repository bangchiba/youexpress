<?php

namespace App\Transformers;

use App\Master_space;
use League\Fractal\TransformerAbstract;

class Space_price_transformer extends TransformerAbstract{

    public function transform(Master_space $space)
    {
        return [
            'id' => $space->id,
            'block_name' => $space->master_block->name,
            'space' => $space->space,
            'master_plan' => $space->master_plan,
            // 'price' => $space->master_plan->pivot->price
        ];
    }
}