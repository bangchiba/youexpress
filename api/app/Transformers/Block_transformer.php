<?php

namespace App\Transformers;

use App\Master_block;
use League\Fractal\TransformerAbstract;

class Block_transformer extends TransformerAbstract{

    public function transform(Master_block $block)
    {
        return [
            'id' => $block->id,
            'name' => $block->name
        ];
    }
}