<?php

namespace App\Transformers;

use App\Master_space;
use League\Fractal\TransformerAbstract;

class Space_transformer extends TransformerAbstract{

    public function transform(Master_space $space)
    {
        return [
            'id' => $space->id,
            'master_block_id' => $space->master_block_id,
            'block_name' => $space->master_block->name,
            'space' => $space->space,
            'long' => $space->long,
            'width' => $space->width,
            'height' => $space->height,
            'status' => $space->getStatusStatusAttribute(),
            'note' => $space->note,
        ];
    }
}