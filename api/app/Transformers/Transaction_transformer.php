<?php

namespace App\Transformers;

use App\Transaction;
use League\Fractal\TransformerAbstract;

class Transaction_transformer extends TransformerAbstract{

    public function transform(Transaction $transaction)
    {
        return [
            'id' => $transaction->id,
            'customer_name' => $transaction->master_customer->name,
            'space_name' => $transaction->master_space->space,
            'plan_name' => $transaction->master_plan->name,
            'start_date' => $transaction->start_date,
            'end_date' => $transaction->end_date,
            'status' => $transaction->getStatusStatusAttribute(),
        ];
    }
    
}