<?php

namespace App\Transformers;

use App\Master_customer;
use League\Fractal\TransformerAbstract;

class Customer_transformer extends TransformerAbstract{

    public function transform(Master_customer $customer)
    {
        return [
            'id' => $customer->id,
            'name' => $customer->name,
            'address' => $customer->address,
            'phone' => $customer->phone,
            'email' => $customer->email,
        ];
    }
}