<?php

namespace App\Transformers;

use App\Master_plan;
use League\Fractal\TransformerAbstract;

class Plan_transformer extends TransformerAbstract{

    public function transform(Master_plan $plan)
    {
        return [
            'id' => $plan->id,
            'name' => $plan->name
        ];
    }
}