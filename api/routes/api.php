<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login', 'Api\UserController@login');
// Route::get('/', 'Api\CustomerController@index');

// Route::get('/transactions', 'Api\TransactionController@index');

Route::resource('block', 'Api\BlockController');
Route::resource('space', 'Api\SpaceController');
Route::resource('plan', 'Api\PlanController');
Route::resource('price', 'Api\PriceController');
Route::resource('customer', 'Api\CustomerController');
Route::resource('transaction', 'Api\TransactionController');

// GET OPTION / SELECT
Route::get('/option_space', 'Api\SpaceController@option_space');
Route::get('/option_plan', 'Api\PlanController@option_plan');