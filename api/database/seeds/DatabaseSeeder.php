<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    protected $toTruncate = [
        'users', 
        'master_plans', 
        'master_blocks',
        'master_spaces',
        'master_space_plan_prices',
        'master_customers'
    ];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        foreach($this->toTruncate as $table){
            DB::table($table)->truncate();
        }
        
        $this->call(UsersTableSeeder::class);
        $this->call(MasterPlanSeeder::class);
        $this->call(MasterBlocksTableSeeder::class);
        $this->call(MasterSpacesTableSeeder::class);
        $this->call(MasterSpacePlanPricesTableSeeder::class);
        $this->call(MasterCustomersTableSeeder::class);
    }
}
