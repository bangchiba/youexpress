<?php

use Illuminate\Database\Seeder;
use App\Master_block;

class MasterBlocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_block = ['A','B','C','D'];

        foreach($arr_block as $val){
            $master_block = [
                'name' => $val
            ];
            Master_block::create($master_block);
        }
    }
}
