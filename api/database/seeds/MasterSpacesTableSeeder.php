<?php

use Illuminate\Database\Seeder;
use App\Master_space;

class MasterSpacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_space = [
             [
                'master_block_id' => 1,
                'space' => 'Ruang A',
                'long' => 30,
                'width' => 20,
                'height' => 10,
                'status' => 1
            ],
            [
                'master_block_id' => 2,
                'space' => 'Ruang B',
                'long' => 30,
                'width' => 20,
                'height' => 10,
                'status' => 1
            ]
        ];

        foreach($arr_space as $val){
            Master_space::create($val);
        }
    }
}
