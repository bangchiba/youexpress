<?php

use Illuminate\Database\Seeder;
use App\Master_customer;

class MasterCustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_customer = [
            [
                'name' => "Khairul Bahri",
                'address' => "Kelapa Gading",
                'phone' => "085360006280",
                'email' => "khairulbahri.aceh@gmail.com",
            ],
            [
                'name' => "Khairul",
                'address' => "Cempaka Putih",
                'phone' => "085277634023",
                'email' => "khairulbahri@gmail.com",
            ],
       ];

       foreach($arr_customer as $val){
        Master_customer::create($val);
       }
    }
}
