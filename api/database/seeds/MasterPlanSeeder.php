<?php

use Illuminate\Database\Seeder;
use App\Master_plan;

class MasterPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_plan = ['Harian','Mingguan','Bulanan','Tahunan'];

        foreach($arr_plan as $val){
            $master_plan = [
                'name' => $val
            ];
            Master_plan::create($master_plan);
        }
    }
}
