<?php

use Illuminate\Database\Seeder;
use App\Master_space_plan_price;

class MasterSpacePlanPricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_space = [
            [
               'master_space_id' => 1,
               'master_plan_id' => 1,
               'price' => 100000
           ],
           [
               'master_space_id' => 1,
               'master_plan_id' => 2,
               'price' => 200000
           ],
           [
            'master_space_id' => 2,
            'master_plan_id' => 1,
            'price' => 100000
        ],
        [
            'master_space_id' => 2,
            'master_plan_id' => 2,
            'price' => 150000
        ]
       ];

       foreach($arr_space as $val){
        Master_space_plan_price::create($val);
       }
    }
}
