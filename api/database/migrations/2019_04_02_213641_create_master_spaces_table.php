<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_spaces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('master_block_id')->unsigned();
            $table->string('space');
            $table->integer('long')->default(0);
            $table->integer('width')->default(0);
            $table->integer('height')->default(0);
            $table->tinyInteger('status')->comment('1: Isi; 2:Kosong');
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_spaces');
    }
}
